Nymph
=====

Nymph is a C++-based data analysis framework.


Dependencies
------------

**External**
- CMake (3.1 or better)
- Boost (date_time, filesystem, program_options, system, thread; 1.46 or better)

**Distributed Code** (included with Mantis directly)
- RapidJSON
- RapidXML


Operating System Support
------------------------

* Mac OS X (usually tested on OS X 10.10)
* Linux (usually tested on Debian Wheezy)


Directory Structure
-------------------

*  cmake - Files that CMake uses to build KTCore.
*  Executables - Source code for the main Katydid executable and test programs.
*  External - RapidJSON and RapidXML packages
*  Library - Source for the KTCore library
*  Templates - Example files for creating a processor and the main CMakeLists.txt file for a package using KTCore.


Installing
----------


Instructions for Use
--------------------


Documentation
-------------



Development
-----------

The Git workflow used is git-flow:
* http://nvie.com/posts/a-successful-git-branching-model/
We suggest that you use the aptly-named git extension, git-flow, which is available from commonly-used package managers:
* https://github.com/nvie/gitflow

Issues should be posted via [GitHub](https://github.com/project8/nymph/issues).